public class TestArrays {
    public static void main(String[] args) {
        String[] arrayOfAccounts = {"", "", "", "", ""};
        double[] amounts = {23, 5444, 2, 345, 34};
        String[] names = {"Picard", "Rtker", "Worf", "Troy", "Data"};

        for(int i = 0; i <arrayOfAccounts.length; i++){
            arrayOfAccounts[i] = amounts[i] + " " + names[i];
            System.out.println(arrayOfAccounts[i]);
        }
    }
}

public class Account {
    String[] name = {};
    double[] balance = {};

    private Account()
        : this("Rachel", 50)
    {

    }

    private Account(String name, Double balance)
    {
        this.name = name;
        this.balance = balance;
    }
}
